**Mini Assignment #1**
```python
age = 21
greeting = "Hello, World!"
pi = 3.14
```

**Mini Assignment #2**
```python
colors = ["red", "blue", "green"]
person = {"name": "John", "age": 30}
```

**Mini Assignment #3**
```python
def add_numbers(num1, num2):
    return num1 + num2

print(add_numbers(3, 5))
```

**Mini Assignment #4**
```python
name = input("Enter your name: ")
color = input("Enter your favorite color: ")
print(f"{name}'s favorite color is {color}.")
```

**Mini Assignment #5**
```python
number = int(input("Enter a number: "))
if number % 2 == 0:
    print("The number is even.")
else:
    print("The number is odd.")
```

**Assignment 1**
```python
num1 = float(input("Enter the first number: "))
num2 = float(input("Enter the second number: "))
operation = input("Enter the operation (add, subtract, multiply, divide): ")

if operation == "add":
    result = num1 + num2
elif operation == "subtract":
    result = num1 - num2
elif operation == "multiply":
    result = num1 * num2
elif operation == "divide":
    if num2 != 0:
        result = num1 / num2
    else:
        result = "Error! Division by zero."

print("The result is: ", result)
```

**Assignment 2**
```python
def personalized_greeting(name, book):
    return f"Hello, {name}. Your favorite book is {book}."

name = input("Enter your name: ")
book = input("Enter your favorite book: ")

print(personalized_greeting(name, book))
```

**Assignment 3**
```python
temperature = float(input("Enter the current temperature: "))

if temperature > 30:
    print("It's so hot today!")
elif temperature < 10:
    print("It's chilly outside!")
else:
    print("The weather is moderate.")
```

**Assignment 4**
```python
def print_board(board):
    for row in board:
        print(row)

def check_win(board):
    for i in range(3):
        if board[i][0] == board[i][1] == board[i][2] != " " or board[0][i] == board[1][i] == board[2][i] != " ":
            return True
    if board[0][0] == board[1][1] == board[2][2] != " " or board[0][2] == board[1][1] == board[2][0] != " ":
        return True
    return False

def tictactoe():
    board = [[" "]*3 for _ in range(3)]
    players = ["X", "O"]

    for i in range(9):
        print_board(board)
        current_player = players[i%2]
        while True:
            row = int(input(f"Enter the row for player {current_player} (0, 1, 2): "))
            col = int(input(f"Enter the column for player {current_player} (0, 1, 2): "))
            if row in [0, 1, 2] and col in [0, 1, 2] and board[row][col] == " ":
                board[row][col] = current_player
                break
            else:
                print("Invalid move, try again.")
        if check_win(board):
            print(f"Player {current_player} wins!")
            print_board(board)
            break
    else:
        print("It's a draw!")
        print_board(board)

tictactoe()
```